<?php
require __DIR__ . "/answer.php";
require __DIR__."/_config.php";

$meterName = filter_input(INPUT_GET, "meterName", FILTER_SANITIZE_STRING);
$currentWatt = filter_input(INPUT_GET, "currentWatt", FILTER_VALIDATE_FLOAT);
$impulsesPerKWH = filter_input(INPUT_GET, "impulsesPerKWH", FILTER_VALIDATE_INT);
$overallImpulses = filter_input(INPUT_GET, "overallImpulses", FILTER_VALIDATE_INT);
/*
 * Helper functions
 */
function return_and_exit(Answer $answer)
{
    header("Content-Type: application/json");
    echo json_encode($answer);
    exit(0);
}

function fail($reason)
{
    $answer = new Answer();
    $answer->success = false;
    $answer->errorMessage = $reason;
    return_and_exit($answer);
}

function succeed($message, $data = null)
{
    $answer = new Answer();
    $answer->success = true;
    $answer->message = $message;
    $answer->data = $data;
    return_and_exit($answer);
}


/*
 * API logic
 */
if (!$meterName || !isset($meterName)) {
    fail("MeterName is not set");
}

if (!isset($currentWatt) || $currentWatt < 0) {
    fail("Watt is below zero. This cannot be correct!");
}

if (!isset($impulsesPerKWH) || $impulsesPerKWH <= 0) {
    fail("Impulses per kWh not set or below or equal zero");
}

if (!isset($overallImpulses) || $overallImpulses < 0) {
    fail("overallImpulses is not set");
}

$mysql = mysqli_connect(CONFIG_MYSQL_HOST, CONFIG_MYSQL_USER, CONFIG_MYSQL_PASSWORD, CONFIG_MYSQL_DB);
if(!$mysql){
    fail(mysqli_connect_error());
}

$qMeter = $mysql->query("SELECT meter_id FROM meters WHERE meter_name = '" . $mysql->escape_string($meterName) . "'");
if(!$qMeter){
    fail("Unable to select meter: " . $mysql->error);
}
$meterId = null;
if($qMeter->num_rows == 0) {
    $qInsertMeter = $mysql->query("INSERT INTO meters (meter_name, meter_full_name, imp_per_kwh) 
      VALUES('".$mysql->escape_string($meterName)."', '".$mysql->escape_string($meterName)."', '".$mysql->escape_string($impulsesPerKWH)."')");
    if(!$qInsertMeter){
        fail("Unable to insert meter: " . $mysql->error);
    }
    $meterId = $mysql->insert_id;
} else {
    $rMeter = $qMeter->fetch_assoc();
    $meterId = $rMeter['meter_id'];
}
$q = "INSERT INTO impulse_stats (meter_id, watt) VALUES('".$mysql->escape_string($meterId)."', ".((float)$currentWatt).")";
echo $q;
$qInsertImpulse = $mysql->query($q);
if(!$qInsertImpulse){
    fail("Unable to insert meter: " . $mysql->error);
}
$impulseId = $mysql->insert_id;
succeed("updated", array("meter_id" => $meterId, "impulseId" => $impulseId));