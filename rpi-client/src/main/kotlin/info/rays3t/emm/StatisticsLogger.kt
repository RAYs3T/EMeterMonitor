package info.rays3t.emm

interface StatisticsLogger {
    fun logStatistics (currentWatt: Double, impulsesPerKWH: Int, timeDifferenceToLastImpulse: Double, overallImpulseCount: Int, meterName: String): Boolean
}