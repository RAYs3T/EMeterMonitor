package info.rays3t.emm

import com.lexicalscope.jewel.cli.Option

open interface StartupParameter {
    @Option
    fun meterName(): String

    @Option
    fun impulsesPerKWH() : Int

    @Option
    fun gpioPin(): Int

    @Option
    fun apiUrl() : String

    @Option(helpRequest = true)
    fun getHelp(): Boolean
}