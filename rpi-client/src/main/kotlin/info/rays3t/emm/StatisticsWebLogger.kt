package info.rays3t.emm

import okhttp3.OkHttpClient
import okhttp3.Request

class StatisticsWebLogger(private val apiUrl: String) : StatisticsLogger {

    val http = OkHttpClient()

    override fun logStatistics(currentWatt: Double, impulsesPerKWH: Int, timeDifferenceToLastImpulse: Double, overallImpulseCount: Int, meterName: String) : Boolean {
        val request = Request
                .Builder()
                .url(apiUrl + "?meterName=$meterName&currentWatt=$currentWatt&impulsesPerKWH=$impulsesPerKWH&overallImpulses=$overallImpulseCount")
                .build()
        println("Executing HTTP Request: ${request.url()} ...")
        val response = http.newCall(request).execute()
        return response.code() == 200
    }

}