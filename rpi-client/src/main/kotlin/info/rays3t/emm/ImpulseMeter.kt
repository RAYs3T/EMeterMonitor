package info.rays3t.emm

open class ImpulseMeter
/**
 * The meter needs to know what the impulse rate of the physical meter is.
 * Normally energy meters are rated with values like (1000 imp / 1 kWh) or (1600 imp / kWh).
 *
 * For logging proposes we need a name. This name is used by the server for distinguish multiple meters.
 *
 * @param impulsesPerKWH Count of impulses per kWh the energy meter is sending
 * @param meterName Name of this meter. The name is used to talk to the statistic backend.
 */(private var impulsesPerKWH: Int, private val meterName: String) {

    private var overallImpulses: Int = 0
    private var lastImpulseTimeMS: Double = 0.0
    private var wattPerImpulse: Double = 0.0
    private var impulsePartSeconds: Double = 0.0


    /**
     * This should be called when the physical meter generated an impulse
     * We using time to calculate the current power consumption between two impulses.
     */
    fun onImpulse() {
        val now = System.currentTimeMillis()

        overallImpulses++

        val differenceSecs: Double = (now - lastImpulseTimeMS) / 1000.0


        val loadLWH = impulsePartSeconds / differenceSecs
        val watt = loadLWH * 1000

        if (lastImpulseTimeMS == -0.0) {
            println("First tick occurred, waiting for next one to calculate ...")
        } else {

            println()
            println(" --- ")
            println("differenceSecs: $differenceSecs")
            println("Impulses per kWh: $impulsesPerKWH")
            println("Watt per Impulse: $wattPerImpulse")
            println("kWh: $loadLWH")
            println("Watt: $watt")
            println("Imp/kWh: $impulsesPerKWH")

            logStatistics(watt, impulsesPerKWH, differenceSecs, overallImpulses, meterName)
        }
        lastImpulseTimeMS = now.toDouble()
    }


    /**
     * This method is intended to be overridden by implementing class.
     * Here some communication to the server should be done.
     *
     * @param currentWatt Current energy consumption
     * @param impulsesPerKWH The amount of impulses this energy meter is configured
     * @param timeDifferenceToLastImpulse time difference in seconds between the last and current impulse
     * @param overallImpulseCount The amount of impulses tracked since the program was started
     * @param meterName Name of the meter
     */
    @Suppress("UNUSED_PARAMETER")
    open fun logStatistics(currentWatt: Double, impulsesPerKWH: Int, timeDifferenceToLastImpulse: Double, overallImpulseCount: Int, meterName: String){
        // not implemented
    }

    init {
        wattPerImpulse = 1000.0 / impulsesPerKWH
        impulsePartSeconds = (60.0 * 60.0) / impulsesPerKWH
        println("Impulses per kWh: $impulsePartSeconds")
    }

}