package info.rays3t.emm

import com.lexicalscope.jewel.cli.CliFactory
import com.pi4j.wiringpi.Gpio
import com.pi4j.wiringpi.GpioInterruptCallback


fun main(args: Array<String>) {

    if(args.isEmpty()){
        error("Please provide arguments!")
        return
    }
    val parsedArguments = CliFactory.parseArguments(StartupParameter::class.java, *args)

    val ticksPerKWH = parsedArguments.impulsesPerKWH()
    val impulsePin = parsedArguments.gpioPin()

    println("Prepare WiringPI ...")

    // setup wiringPi
    if (Gpio.wiringPiSetupSys() == -1) {
        error(" ==>> GPIO SETUP FAILED")
        return
    }

    println("Setting up pin")
    Gpio.pinMode(impulsePin, Gpio.INPUT)
    Gpio.pullUpDnControl(impulsePin, Gpio.PUD_UP)

    println("Initialising ImpulseMeter ...")
    val logger = StatisticsWebLogger(parsedArguments.apiUrl())

    val im = object : ImpulseMeter(ticksPerKWH, parsedArguments.meterName()) {
        override fun logStatistics(currentWatt: Double, impulsesPerKWH: Int, timeDifferenceToLastImpulse: Double, overallImpulseCount: Int, meterName: String) {
            val logSucceeded = logger.logStatistics(currentWatt, impulsesPerKWH, timeDifferenceToLastImpulse, overallImpulseCount, meterName);
            if (logSucceeded) {
                println("Logged!")
            } else {
                println("Unable to log!")
            }
        }
    }

    var callback = GpioInterruptCallback {
        im.onImpulse()
    }

    Gpio.wiringPiISR(impulsePin, Gpio.INT_EDGE_FALLING, callback)



    println("Listen for signals ...")
    readLine()

    println("Shutting down ...")

}